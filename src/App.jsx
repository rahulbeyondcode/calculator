import React, { Component } from "react";
import "./App.css";
import math from "mathjs";
import Icon from "antd/lib/icon";

class App extends Component {
  constructor() {
    super();
    this.state = {
      value: ""
    };
  }
  render() {
    return (
      <>
        <div className="Header">
          <h1>Calculator</h1>
        </div>
        <div className="container-fluid" style={{ userSelect: "none" }}>
          <div className="row justify-content-center mt-5">
            <div className="col-sm-7 col-md-6 col-lg-5 col-xl-4">
              {/* Final Answer Display Box */}
              <div className="row border border-secondary control-overflow  bg-light">
                <div className="col text-right p-4">
                  <h1>{this.state.value === "" ? 0 : this.state.value}</h1>
                </div>
              </div>
              {/* Second Row with clear button, erase button and division operator */}
              <div className="row border border-secondary">
                <div
                  className="col-6 border-right pt-4 text-center"
                  onClick={() => this.setState({ value: "" })}
                >
                  <h1>Clear</h1>
                </div>
                <div
                  className="col-3 border-right pt-4 text-center"
                  onClick={() =>
                    this.setState({ value: this.state.value.slice(0, -1) })
                  }
                >
                  <h1>
                    <Icon type="left-circle" />
                  </h1>
                </div>
                <div
                  className="col-3 p-4 bg-danger text-white align-self-center text-center"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}/` })
                  }
                >
                  <h1>&divide;</h1>
                </div>
              </div>
              {/* 9 8 and 7 along with multiplication operator */}
              <div className="row border border-secondary">
                <div
                  className="col-3 border-right"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}7` })
                  }
                >
                  <h1 className="p-4">7</h1>
                </div>
                <div
                  className="col-3 border-right"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}8` })
                  }
                >
                  <h1 className="p-4">8</h1>
                </div>
                <div
                  className="col-3 border-right"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}9` })
                  }
                >
                  <h1 className="p-4">9</h1>
                </div>
                <div
                  className="col-3 p-4 bg-danger text-white align-self-center text-center"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}*` })
                  }
                >
                  <h1>&times;</h1>
                </div>
              </div>
              {/* 6 5 and 4 along with subtraction operator */}

              <div className="row border border-secondary">
                <div
                  className="col-3 border-right"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}4` })
                  }
                >
                  <h1 className="p-4">4</h1>
                </div>
                <div
                  className="col-3 border-right"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}5` })
                  }
                >
                  <h1 className="p-4">5</h1>
                </div>
                <div
                  className="col-3 border-right"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}6` })
                  }
                >
                  <h1 className="p-4">6</h1>
                </div>
                <div
                  className="col-3 p-4 bg-danger text-white align-self-center text-center"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}-` })
                  }
                >
                  <h1>-</h1>
                </div>
              </div>
              {/* 3 2 and 1 along with addition operator */}

              <div className="row border border-secondary">
                <div
                  className="col-3 border-right"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}1` })
                  }
                >
                  <h1 className="p-4">1</h1>
                </div>
                <div
                  className="col-3 border-right"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}2` })
                  }
                >
                  <h1 className="p-4">2</h1>
                </div>
                <div
                  className="col-3 border-right"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}3` })
                  }
                >
                  <h1 className="p-4">3</h1>
                </div>
                <div
                  className="col-3 p-4 bg-danger text-white align-self-center text-center"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}+` })
                  }
                >
                  <h1>+</h1>
                </div>
              </div>
              {/* Last row with 0 decimal and = operator */}

              <div className="row border border-secondary">
                <div
                  className="col-6 border-right text-center"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}0` })
                  }
                >
                  <h1 className="p-4">0</h1>
                </div>
                <div
                  className="col-3 border-right"
                  onClick={() =>
                    this.setState({ value: `${this.state.value}.` })
                  }
                >
                  <h1 className="p-4">.</h1>
                </div>
                <div
                  className="col-3 p-4 bg-success text-white align-self-center text-center"
                  onClick={() => {
                    this.setState({
                      value: (
                        Math.round(math.eval(`${this.state.value}`) * 100) / 100
                      ).toString()
                    });
                  }}
                >
                  <h1>=</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default App;
